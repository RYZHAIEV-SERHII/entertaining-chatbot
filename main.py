import pyjokes
from art import *
from colorama import Fore, Style

from games import numerical_guess, rock_paper_scissors, wonderfield

NAME = ""


def greeting():
    """
    Greets the user and prompts for their name.
    """
    global NAME

    tprint("Welcome to the Entertaining ChatBot", font="white-square")

    # Prompt user for their name, ensuring it's not empty
    while True:
        print("\n🆔 Introduce yourself please: ")
        NAME = input("▶ ").strip()
        if NAME:
            break
        else:
            print(Fore.RED + "Please enter a valid name." + Style.RESET_ALL)

    print(
        f"I was created for bringing joy, so I'll do my best to make you happy, {Fore.GREEN}{NAME}{Style.RESET_ALL}!"
    )


def recommendations():
    """Recommendations system which basing on the request from the user give some recommendations"""

    while True:
        print("What recommendations do you want?")
        print("1️⃣ Interesting movies 📽")
        print("2️⃣ Cool music 🎧")
        print("3️⃣ Awesome video games 🎮")
        print("0️⃣ Back to the main menu ↩")

        choice = input("▶ ")

        match choice:
            case "1":
                pass
            case "2":
                pass
            case "3":
                pass
            case "0":
                print(f"Come again {NAME} if you would need some recommendations")
                break
            case _:
                print("Incorrect choice!")
                print("Please select from the available options")


def jokes():
    """Tells a random jokes"""

    print(pyjokes.get_joke())
    while True:
        choice = input("Wanna more jokes? (y/n): ").lower()
        match choice:
            case "y":
                print(pyjokes.get_joke())
            case "n":
                print(f"I hope you have had fun {NAME}, and your mood become better!?")
                print("See you later!")
                break
            case _:
                print("Incorrect choice!")
                print("y = yes\n"
                      "n = no")


def games():
    """Games menu in which user can choose some game to play"""

    while True:
        print("Available mini games: ")
        print("1️⃣ Numerical guess")
        print("2️⃣ Rock-Paper-Scissors")
        print("3️⃣ Wonder field")
        print("0️⃣ Back to the main menu ↩")

        choice = input("▶ ")

        match choice:
            case "1":
                numerical_guess.play()
            case "2":
                rock_paper_scissors.play()
            case "3":
                wonderfield.play()
            case "0":
                print(f"Come to play again {NAME}")
                break
            case _:
                print("Incorrect choice!")
                print("Please select from the available games")


def menu():
    """Main Entertaining ChatBot menu"""

    while True:
        print("Choose option below: ")
        print("1️⃣ Give recommendations 🔝")
        print("2️⃣ Tell a joke 🤡")
        print("3️⃣ Tell some interesting story 💬")
        print("4️⃣ Play mini game 🎮")
        print("0️⃣ Exit ❌")

        choice = input("▶ ")

        match choice:
            case "1":
                recommendations()
            case "2":
                jokes()
            case "3":
                pass
            case "4":
                games()
            case "0":
                tprint("See you later!", font="white-square")
                break
            case _:
                print("Wrong choice!")
                print("Please select a number from menu.")


if __name__ == "__main__":
    greeting()
    menu()
