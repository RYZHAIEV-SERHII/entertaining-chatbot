import random

from art import tprint


def play_game():
    """
    Plays the number guessing game.
    """

    attempt_count = 0
    boundary = get_boundary()
    number = random.randint(1, boundary + 1)

    while True:
        response = input(f"Enter a number from 1 to {boundary}: ▶ ")
        if not is_valid(response, boundary):
            print("Please enter an integer from 1 to", boundary)
            continue
        val = int(response)

        if val < number:
            attempt_count += 1
            print("🔽 Your number is lower than the hidden one, try again")
        elif val > number:
            attempt_count += 1
            print("🔼 Your number is higher than the hidden one, try again")
        else:
            attempt_count += 1
            print("🎉 Congratulations, you guessed it!")
            print(f"You needed {attempt_count} attempts to guess the number!")
            break


def get_boundary():
    """
    Prompts the user for the upper boundary of the number to guess.
    """

    while True:
        boundary = input("Enter the upper boundary for the number to guess: ▶ ")
        if boundary.isdigit():
            return int(boundary)
        else:
            print("⚠ Please enter a valid integer.")


def is_valid(num, boundary):
    """
    Checks if the input is a valid integer within the specified boundary.
    """

    if num.isdigit():
        num = int(num)
        if 1 <= num <= boundary:
            return True
    return False


def play_again():
    """
    Asks the user if they want to play again.
    """

    while True:
        answer = input(
            "Do you want to play again?\n"
            "1️⃣ Play again \n"
            "0️⃣ Quit the game ❎\n"
            "Enter your choice: ▶ "
        )
        if answer == "1":
            play_game()
        elif answer == "0":
            break
        else:
            print("⚠ Invalid choice.")


if __name__ == "__main__":
    tprint("Welcome to the Number Guessing Game", font="white-square")
    print("-" * 55)
    play_game()
    play_again()
    print("Thank you for playing the Number Guessing Game. See you next time!")
